﻿using Coffee_Club.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Coffee_Club
{
    public class CustomGlobal : UmbracoApplication
    {
        public void Init(HttpApplication application)
        {
            application.PreRequestHandlerExecute += application_PreRequestHandlerExecute;
            application.BeginRequest += this.Application_BeginRequest;
            application.EndRequest += this.Application_EndRequest;
            application.Error += Application_Error;
        }

        protected override void OnApplicationStarted(object sender, EventArgs e)
        {
            //DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(CustomController));
            base.OnApplicationStarted(sender, e);

            // Your code here
        }

        private void application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            try
            {
                if (Session != null && Session.IsNewSession)
                {
                    // Your code here
                }
            }
            catch (Exception ex) { }
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                // You begin request code here
            }
            catch { }
        }

        private void Application_EndRequest(object sender, EventArgs e)
        {
            // Your code here
        }

        protected new void Application_Error(object sender, EventArgs e)
        {
            // Your error handling here
        }
    }
}