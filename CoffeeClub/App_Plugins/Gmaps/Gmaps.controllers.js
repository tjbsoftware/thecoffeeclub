﻿ angular.module("umbraco").controller("GMaps.GoogleMapsController",
     function ($rootScope, $scope, notificationsService, dialogService, assetsService) {

        assetsService.loadJs('https://www.google.com/jsapi')
            .then(function () {
                google.load("maps", "3",
                            {
                            callback: initMap,
                            other_params: "key=AIzaSyABG1Dr54I63kK7UnYE2M-PKcVBcwQDVBs&sensor=false&libraries=geometry&"
                             });
             });

        function initMap() {
            
            
            notificationsService.warning("GMaps", "Started initMap()");
            var geocoder = new google.maps.Geocoder();            

            var address1 = document.getElementById('addressLine2').value;
          //  var address2 = document.getElementById('addressLine3').value;
            var address = address1 ;

            //var latlog = '-36.850718,174.7593026';
            if (!address1.match(/\S/)) {

                var latlog = '-36.850718,175.7593028';
                $scope.model.value = latlog;

                console.log($scope.model.value + " textbox empty");
                var valueArray = $scope.model.value.split(',');
                var latLng = new google.maps.LatLng(valueArray[0], valueArray[1]);

                var mapDiv = document.getElementById($scope.model.alias + '_map');

                var mapOptions = {
                    zoom: 12,
                    center: latLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var geocoder = new google.maps.Geocoder();
                var map = new google.maps.Map(mapDiv, mapOptions);

                var marker = new google.maps.Marker({
                    map: map,
                    position: latLng,
                    draggable: true
                });

                google.maps.event.addListener(marker, "dragend", function (e) {
                    var newLat = marker.getPosition().lat();
                    var newLng = marker.getPosition().lng();

                    codeLatLng(marker.getPosition(), geocoder);

                    //set the model value
                    $scope.model.value = newLat + "," + newLng;
                });

                $('a[data-toggle="tab"]').on('shown', function (e) {
                    google.maps.event.trigger(map, 'resize');
                });

                                    


            } else {

                var firstStageLatlon = $scope.model.value
                console.log(address);
                console.log(firstStageLatlon + ' firstStageLatlon');

                geocoder.geocode({ 'address': address + ', NZ' }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        var string1 = latitude + ',' +longitude;
                      
                        console.log($scope.model.value + "check if empty");
                        console.log(string1 + " string 1");                       
                      
                        console.log(firstStageLatlon + ' before comparison');


                        if ($scope.model.value == '-36.850718,175.7593028' ) {
                            $scope.model.value = string1
                        }

                        
                       

                         
                         console.log($scope.model.value + " Right before assigning");
                        var valueArray = $scope.model.value.split(',');
                        var latLng = new google.maps.LatLng(valueArray[0], valueArray[1]);



                        //notificationsService.warning("GMaps", "Latitude=" + valueArray[0]);
                        //notificationsService.warning("GMaps", "Longitude=" + valueArray[1]);
                        ///////////////////////////////////////////////////////////////////////////
                        var mapDiv = document.getElementById($scope.model.alias + '_map');

                        var mapOptions = {
                            zoom: 12,
                            center: latLng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };

                        var geocoder = new google.maps.Geocoder();
                        var map = new google.maps.Map(mapDiv, mapOptions);

                        var marker = new google.maps.Marker({
                            map: map,
                            position: latLng,
                            draggable: true
                        });
                        /////////////////////////////////////////////////////////////////////////////////
                        //notificationsService.warning("GMaps", "Finished initMap()");

                        google.maps.event.addListener(marker, "dragend", function (e) {
                            var newLat = marker.getPosition().lat();
                            var newLng = marker.getPosition().lng();

                            codeLatLng(marker.getPosition(), geocoder);

                            //set the model value
                            $scope.model.value = newLat + "," + newLng;
                        });

                        $('a[data-toggle="tab"]').on('shown', function (e) {
                            google.maps.event.trigger(map, 'resize');
                        });






                    }                    
                });

            }

            //if ($scope.model.value === '') {
            //    $scope.model.value = '-36.850718,174.7593026';
            //}



            //var valueArray = $scope.model.value.split(',');
            //var latLng = new google.maps.LatLng(valueArray[0], valueArray[1]);

              

           
            

            
        }

    function codeLatLng(latLng, geocoder) {
            geocoder.geocode({ 'latLng': latLng },
                function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                                   var location = results[0].formatted_address;
                                    $rootScope.$apply(function () {
                                            notificationsService.success("Selected location: ", location);
                                        });
                     } else {
                         notificationsService.error("error");
                     }
             });
    }
         
    //(function ($) {
    //    $(document).ready(function () {

    //        $('#addressLine3').blur(function () {

    //            initMap()
    //        });

    //    });
    //})(jQuery);




    
     });

 
